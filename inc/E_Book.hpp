#ifndef E_BOOK_HPP
#define E_BOOK_HPP

#include <string.h>
#include "Produto.hpp"

using namespace std;

 class E_Book : public Produto {
 
     public:
        E_Book();
        E_Book(string nome,string quantidade,string categoria,string preco);

        void Cadastra_e_book(vector<E_Book * > &lista_e_book);
        void escrever_arquivo_ebook(vector<E_Book * > &lista_e_book,int tam);
        void Lendo_arquivo_e_book(vector<E_Book * > &lista_e_book);
        void imprimi_Produtos(vector<E_Book * > &lista_e_book);
 };

#endif
