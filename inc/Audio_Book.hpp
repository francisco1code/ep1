#ifndef AUDIO_BOOK_HPP
#define AUDIO_BOOK_HPP
#include <string>
#include "Produto.hpp"
#include <vector>

 using namespace std;

 class Audio_Book : public Produto {
     
     public:
         Audio_Book();
         Audio_Book(string nome,string quantidade,string categoria,string preco);

         void Cadastra_audio_book(vector<Audio_Book * > &lista_audio_book);
         void escrever_arquivo_audio_book(vector<Audio_Book * > &lista_audio_book,int tam);
         void Lendo_arquivo_audio_book(vector<Audio_Book * > &lista_audio_book);
         void imprimi_Produtos_audio(vector<Audio_Book * > &lista_audio_book);
 };

#endif
