#ifndef CLIENTE_HPP
#define CLIENTE_HPP

#include "Pessoa.hpp"
#include <string>
#include <vector>

using namespace std;

class Cliente : public Pessoa  {
    
    public:
        Cliente();
        Cliente(string nome_pessoa,string email,string cpf);

        void Cadastra_cliente( vector<Cliente * > &lista_cliente );
        void escrever_arquivo_Cliente(vector<Cliente * > &lista_cliente,int tam);
        void Lendo_arquivo_cliente(vector<Cliente * > &lista_cliente);
               
};

#endif
