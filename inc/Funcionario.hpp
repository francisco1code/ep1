#ifndef FUNCIONARIO_HPP
#define FUNCIONARIO_HPP
#include <string>
#include "Pessoa.hpp"


 using namespace std;

 class Funcionario : public Pessoa {
     
    public:
        Funcionario();
 
        void verifica_funcionario();
 };
#endif
