#include "Audio_Book.hpp"
#include <bits/stdc++.h>

Audio_Book::Audio_Book(){
    set_nome("");
	set_categoria("");
    set_quantidade("");
    set_preco("");
}
Audio_Book::Audio_Book(string nome,string quantidade, string categoria,string preco){
    set_nome(nome);
	set_quantidade(quantidade);
	set_categoria(categoria);
    set_preco(preco);
}
void Audio_Book::Cadastra_audio_book(vector<Audio_Book * > &lista_audio_book){
		system("clear");
	    const int TAM = 50;
        int cont=0,t=lista_audio_book.size();
        char nome[TAM], categoria[TAM];
        char quantidade[TAM],preco[TAM]; 
        cout<<"Cadastra Produto"<<endl<<endl;
        cout<<"Digite os dados do Audio Book para  o cadastro:"<<endl<<endl;
        cout<<"Digite o nome:"<<endl; 
	    cin.ignore();
        cin.getline(nome,TAM);
        cout<<"Digite a quantidade:"<<endl;
        cin.getline(quantidade,TAM);  
        cout<<"Digite a categoria:"<<endl; 
        cin.getline(categoria,TAM);
        cout<<"Digite o valor do Produto: ";
        cin.getline(preco,TAM);
        system("clear");  
        for(int i = 0; i < t; i++){
            if(nome == lista_audio_book[i]->get_nome()) {
                cout<<"Produto ja cadastrado!"<<endl<<endl;
                cont++;
                break;
            }
        }
        if(cont==0){
       lista_audio_book.push_back(new Audio_Book(nome,quantidade,categoria,preco)); 
       cout<<"Audio Book criado com sucesso!"<<endl;
        }
        cout<<"Aperte qualquer tecla para continuar"<<endl;
        getchar();
        system("clear");
        cont=0;

}
void Audio_Book::escrever_arquivo_audio_book(vector<Audio_Book * > &lista_audio_book,int tam){
    std::fstream f1;
    f1.open("doc/Lista_Audio_Book.txt",ios::app);
    int t = lista_audio_book.size();
    for(int i = tam; i < t; i++){
        f1 << lista_audio_book[i]->get_nome()<<endl;
        f1 << lista_audio_book[i]->get_quantidade()<<endl;
        f1 << lista_audio_book[i]->get_categoria()<<endl;
        f1 << lista_audio_book[i]->get_preco()<<endl;
    }
}
void Audio_Book::Lendo_arquivo_audio_book(vector<Audio_Book * > &lista_audio_book){ 
    lista_audio_book.clear();
    std::ifstream lista_audio_book1("doc/Lista_Audio_Book.txt",ios::in);
    const int TAM = 50;
    char nome1[TAM], quantidade1[TAM], categoria1[TAM],preco1[TAM]; 
    while(!lista_audio_book1.eof()){     
        lista_audio_book1.getline(nome1,TAM);
        lista_audio_book1.getline(quantidade1,TAM);
        lista_audio_book1.getline(categoria1,TAM);
        lista_audio_book1.getline(preco1,TAM);
        lista_audio_book.push_back(new Audio_Book(nome1,quantidade1,categoria1,preco1));
        
    }
} 
void Audio_Book::imprimi_Produtos_audio(vector<Audio_Book * > &lista_audio_book){
    cout<<"##########Audio-Books###########"<<endl;
    int t3=lista_audio_book.size();
    for(int i = 0; i < t3; i++){
        if(!lista_audio_book.empty()){
        cout<< lista_audio_book[i]->get_nome()<<endl;
        cout<< lista_audio_book[i]->get_quantidade()<<endl;
        cout<< lista_audio_book[i]->get_categoria()<<endl; 
        cout<< lista_audio_book[i]->get_preco()<<endl;
        }
    }
    
}

