#include "Cliente.hpp"
#include <bits/stdtr1c++.h>


Cliente::Cliente(){
    set_nome_pessoa(""); 
    set_cpf("");
    set_email("");
}
Cliente::Cliente(string nome_pessoa,string email,string cpf){
     set_nome_pessoa(nome_pessoa); 
     set_cpf(cpf);
     set_email(email);
   
}
void Cliente::Cadastra_cliente(vector<Cliente * > &lista_cliente){
    const int TAM = 50;
    int cont=0,t=lista_cliente.size();
    char nome[TAM], email[TAM], cpf[TAM]; 
       cout<<"Digite os dados do cliente para cadastro:"<<endl<<endl;
       cout<<"Atencao se o cliente nao tiver cadastro sera feito automaticamente!"<<endl;
       cin.ignore();
       cout<<"Digite o CPF do cliente: "; 
       cin.getline(cpf,TAM);
       cout<<"Digite o nome do cliente: ";
       cin.getline(nome,TAM);  
       cout<<"Digite o email do cliente:"; 
       cin.getline(email,TAM);  
       system("clear");
        for(int i = 0; i < t; i++){
            if(cpf == lista_cliente[i]->get_cpf()) {
                cout<<"O cliente ja possui cadastro!"<<endl<<endl;
                cont++;
                break;
            }
        }
        if(cont==0){
        lista_cliente.push_back(new Cliente(nome,email,cpf)); 
        cout<<"Cliente Cadastrado"<<endl;
        }
        cout<<"Aperte qualquer tecla para continuar a compra!"<<endl;
        getchar(); 
        cont=0;
}
void Cliente::escrever_arquivo_Cliente(vector<Cliente * > &lista_cliente,int tam){
    std::fstream file;
    file.open("doc/Lista_cliente.txt",ios::app);
    int t = lista_cliente.size();
    for(int i = tam; i < t; i++){
        file << lista_cliente[i]->get_nome_pessoa()<<endl;
        file << lista_cliente[i]->get_email()<<endl;
        file << lista_cliente[i]->get_cpf()<<endl;
    }
   
}
void Cliente::Lendo_arquivo_cliente(vector<Cliente * > &lista_cliente){
        lista_cliente.clear();
        std::ifstream lista_cliente1("doc/Lista_cliente.txt",ios::in);
        const int TAM = 50;
        char nome1[TAM], email1[TAM], cpf1[TAM];
        while(!lista_cliente1.eof()){ 
        lista_cliente1.getline(nome1,TAM);
        lista_cliente1.getline(email1,TAM);
        lista_cliente1.getline(cpf1,TAM);
        lista_cliente.push_back(new Cliente(nome1,email1,cpf1));

    }

}

