# DOCUMENTAÇAO DO SOFTWARE
## DESCRIÇÃO RAPIDA
Esse software foi baseado em cima do contexto de uma loja de produtos Virtuais, apesar dos produtos serem virtuais, a loja é fisica e o atendimento é feito por funcionarios, ou seja o tempo todo quem vai manusear o software é o funcionario, onde é vendido dos tipos de produtos **E-Book** e **Audio Book**,  tipos de produtos diferentes podem pertencer a mesma categoria.

![LOGO](./doc/imagem_readme/logo.png)

Assim que o software é inicializado é mostrado na tela um logo do softawe , o nome da loja virtual  **X-VICTORIA** ,logo em seguida o **Menu** é aberto!

## MENU PRINCIPAL
![MENU](./doc/imagem_readme/menu.png)

* **MODO VENDA**
  
![Cadastro](./doc/imagem_readme/cadastro_cliente.png)
* * **IDENTIFICAÇÃO DE CLIENTE** -> Aqui você ira inserir os dados dos cliente para saber se ele já e cadastrado , se não, o cadastro é feito automaticamenete ,se sim ele vai avisar que o cliente já possui cadastro.
* * **IMPRIMIR PRODUTO NA TELA** -> Aqui você vai reescrever os produtos em um vector que está integrado ao modo estoque, onde fica armazenado o produto,quantidade e preço.
* * **FINALIZAR COMPRA** -> Esse modo ainda não foi implementado 😢😢😢
* 
* **MODO RECOMENDAÇAO**
* * **IMPRIME A RECOMENDAÇAO** -> Nao foi impementado 😬😬😬
* 

* **MODO ESTOQUE** 

![Estoque](./doc/imagem_readme/menu_estoque.png)

* * **CADASTRA PRODUTO TIPO E-BOOK** -> nesse trecho vai ser solicitado o nome do produto, quantidade,categoria e seu preço.

![cadastro](./doc/imagem_readme/cadastro_produto.png)

* * **CADASTRA PRODUTO TIPO AUDIO-BOOK**
* * **IMPRIMI PRODUTOS** -> Aqui o funcionario faz uma pesquisa rapida para ver os produtos cadastrados.

![imprimi](./doc/imagem_readme/imprimi_produto.png)

* * **IDENTIFICAR O FUNCIONARIO** -> não foi implementado
### EXECUÇÃO
* **EXECUTE O COMANDO make >> PARA COMPILAR TODAS AS PASTAS E CRIAR OS ARQUIVOS.O**
* **EXECUTE O COMANDO make run >> PARA EXECULTAR O PROGRAMA**

### PEDENCIAS
**Verificar se o g++ está intalado na maquina** 

```
$ sudo apt install g++
$ sudo update 
```
**Verificar o include da biblioteca "bits/stdc++.h"**

**ATENÇAO** Todos os comandos acima devem ser realizado no sistema operacional **LINUX**



### DESCRIÇÃO COMO FUNCIONA
**INICIALMENTE É BEM SIMPLES** Todo o sistema foi desenvolvido em cima do paradigma de **Orientação a Objtos**, com intuito de simplificar e deixa o sistema mais robusto,utilizando as tecnicas do paradigma ex. encapsulamento,herença,polimorfismo..., resumindo o projeto em poucas palavras, temos um sistema que recebe dados de cliente produtos e funcionarios e armazena esses dados em um txt, assim mantendo a integralidade dos dados ,ou seja mesmo que o softaware seja fechado na proxima execução  todos os dados são recuperados.









